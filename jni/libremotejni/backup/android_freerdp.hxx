#ifndef __ANDROID_FREERDP_H__
#define __ANDROID_FREERDP_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "thread.h"

extern "C" {

#include <winpr/crt.h>
#include <winpr/synch.h>

#include <freerdp/constants.h>
#include <freerdp/types.h>
#include <freerdp/api.h>
#include <freerdp/freerdp.h>
#include <freerdp/graphics.h>
#include <freerdp/gdi/gdi.h>
#include <freerdp/codec/color.h>
#include <freerdp/codec/nsc.h>
#include <freerdp/codec/rfx.h>
#include <freerdp/locale/keyboard.h>
#include <freerdp/utils/event.h>
#include <freerdp/client/file.h>
#include <freerdp/client/cmdline.h>
#include <freerdp/client/channels.h>
#include <freerdp/client/cliprdr.h>
#include <freerdp/client/drdynvc.h>
#include <freerdp/channels/channels.h>

}

#define RDP_NOP  				0 // interface test message
#define RDP_CONNECTED 			1
#define RDP_DISCONNECTED 		2
#define RDP_UPDATE_SCREEN 		3
#define RDP_SET_DISPLAY_MODE 	4

class AndroidFreeRDPListener {
public:
	virtual void connected() = 0;
	virtual bool verifyCertificate(char* subject, char* issuer, char* fingerprint) = 0;
	virtual void setDisplayMode(int w, int h, int d) = 0;
    virtual void updateScreen(rdpGdi* gdi, int x, int y, int w, int h) = 0;
    virtual void disconnected() = 0;
};

#define ANDROID_RDP_NULL_EVENT  0x00
#define ANDROID_RDP_KEY_EVENT   0x01
#define ANDROID_RDP_MOUSE_EVENT 0x02
#define ANDROID_RDP_TOUCH_EVENT 0x03
#define ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES         7
#define ANDROID_RDP_TOUCH_EVENT_POINTER_ID_OFFSET 	0
#define ANDROID_RDP_TOUCH_EVENT_X_OFFSET 			1
#define ANDROID_RDP_TOUCH_EVENT_Y_OFFSET 			2
#define ANDROID_RDP_TOUCH_EVENT_ORIENTATION_OFFSET 	3
#define ANDROID_RDP_TOUCH_EVENT_PRESSURE_OFFSET 	4
#define ANDROID_RDP_TOUCH_EVENT_TOUCH_MAJOR_OFFSET 	5
#define ANDROID_RDP_TOUCH_EVENT_TOUCH_MINOR_OFFSET 	6
    

typedef struct _Android_Rdp_Key_Event {
	int type;
	int keycode;
	bool unicode;
	UINT16 device_flags;
}Android_Rdp_Key_Event_t;

typedef struct _Android_Rdp_Mouse_Event {
	int type;
	int x;
	int y;
	UINT16 device_flags;
}Android_Rdp_Mouse_Event_t;

typedef struct _Android_Rdp_Touch_Event {
	int type;
	UINT32 contactFlags;
	UINT64 time;
	UINT32 contactCount;
	float dataSample[ANDROID_RDP_TOUCH_EVENT_NUM_SAMPLES * MAX_TOUCH_CONTACTS];
}Android_Rdp_Touch_Event_t;

typedef union _Android_Rdp_Event {
	int type;
	Android_Rdp_Key_Event_t key;
	Android_Rdp_Mouse_Event_t mouse;
	Android_Rdp_Touch_Event_t touch;
} Android_Rdp_Event_t;

class AndroidFreeRDP {
private:
	freerdp* mInstance;
	AndroidFreeRDPListener *mListener;

	Mutex		mLock;
	pthread_t	mThread;
	bool		mStopped;
	bool		mRunning;
	int			mEventPipe[2];

private:
	static void android_context_new(freerdp* instance, rdpContext* context);
	static void android_context_free(freerdp* instance, rdpContext* context);
	static void android_begin_paint(rdpContext* context);
	static void android_end_paint(rdpContext* context);
	static BOOL android_pre_connect(freerdp* instance);
	static BOOL android_post_connect(freerdp* instance);
	static int android_process_plugin_args(rdpSettings* settings, const char* name,
                                   RDP_PLUGIN_DATA* plugin_data, void* user_data);
	static BOOL android_verify_certificate(freerdp* instance, char* subject, char* issuer, char* fingerprint);
	static int android_receive_channel_data(freerdp* instance, int channelId, UINT8* data, int size, int flags, int total_size);
	static void android_process_cb_monitor_ready_event(rdpChannels* channels, freerdp* instance);
	static void android_process_channel_event(rdpChannels* channels, freerdp* instance);
	
	BOOL android_get_fds(void** rfds, int* rcount, void** wfds, int* wcount);
	BOOL android_check_fds(fd_set* set);
	void android_post_event(Android_Rdp_Event_t& event);
	void android_process_mouse_event(Android_Rdp_Mouse_Event_t *event);
	void android_process_key_event(Android_Rdp_Key_Event_t* event);
	void android_process_touch_event(Android_Rdp_Touch_Event_t* event, int count);
	int androidfreerdp_run();
	static void* threadEntry(void* param);	
	
public:
	AndroidFreeRDP();
	virtual ~AndroidFreeRDP();

	void setListener(AndroidFreeRDPListener* listener) { mListener = listener; }
	AndroidFreeRDPListener* getListener() { return mListener; }
	freerdp* getRDPInstance() { return mInstance; }
	
	void parseArgs(int argc, char* argv[]);
	void connect();
	void disconnect();
	void sendMouseEvent(int x, int y, UINT16 device_flags);
	void sendKeyEvent(int keycode, bool unicode, UINT16 device_flags);
	void sendMotionEvent(int contactFlags, UINT64 time, UINT32 contactCount, float dataSample[]);
};

#endif//__ANDROID_FREERDP_H__
