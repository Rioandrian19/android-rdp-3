LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LIBVNCSERVER_ROOT:=.

LOCAL_SRC_FILES:= \
	$(LIBVNCSERVER_ROOT)/libvncclient/cursor.c \
	$(LIBVNCSERVER_ROOT)/libvncclient/listen.c \
	$(LIBVNCSERVER_ROOT)/libvncclient/rfbproto.c \
	$(LIBVNCSERVER_ROOT)/libvncclient/sockets.c \
	$(LIBVNCSERVER_ROOT)/libvncclient/vncviewer.c \
	$(LIBVNCSERVER_ROOT)/common/minilzo.c \
	$(LIBVNCSERVER_ROOT)/libvncclient/tls_openssl.c \

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	$(LOCAL_PATH)/$(LIBVNCSERVER_ROOT)/libclient \
	$(LOCAL_PATH)/$(LIBVNCSERVER_ROOT)/common \
	$(LOCAL_PATH)/$(LIBVNCSERVER_ROOT) \
	$(LOCAL_PATH)/../jpeg \
	$(LOCAL_PATH)/../openssl \
	$(LOCAL_PATH)/../openssl/include \

LOCAL_LDLIBS := -lz
LOCAL_STATIC_LIBRARIES := jpeg crypto ssl crypto

LOCAL_MODULE:= libvncclient

include $(BUILD_STATIC_LIBRARY)
