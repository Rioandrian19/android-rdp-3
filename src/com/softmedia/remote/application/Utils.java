package com.softmedia.remote.application;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.softmedia.remote.R;

public class Utils 
{
	private static final String LOG_TAG = "Utils";
	
	private static final String AD_UNIT_ID = "a14dee3be194878";    	
	
	public static void initializeCheckLicenseOrAd(Activity act) {	
		try
		{
			AdView ad = new AdView(act, AdSize.BANNER, AD_UNIT_ID);
			//Add the adView to it
			ViewGroup v = (ViewGroup) act.findViewById(R.id.ad_container);
			if (v != null)
			{
				v.setVisibility(View.VISIBLE);
				v.addView(ad);
				//Initiate a generic request to load it with an ad
				ad.loadAd(new AdRequest());
			}
		} catch (Throwable t) {
			Log.e(LOG_TAG, "", t);
		}
	}		
}
